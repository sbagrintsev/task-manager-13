package ru.tsc.bagrintsev.tm.enumerated;

public enum Status {
    
    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    public static Status toStatus(String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : Status.values()) {
            if (status.toString().equals(value)) {
                return status;
            }
        }
        return null;
    }

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
