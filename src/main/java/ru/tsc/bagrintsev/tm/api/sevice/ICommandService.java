package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.model.Command;

public interface ICommandService {

    Command[] getAvailableCommands();

}
