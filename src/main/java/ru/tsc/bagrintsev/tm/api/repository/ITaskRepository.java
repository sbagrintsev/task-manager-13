package ru.tsc.bagrintsev.tm.api.repository;

import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;
import java.util.Map;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    Map<Integer, Task> findAllByProjectId(String projectId);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    int taskCount();

    boolean existsById(String id);

    void setProjectId(String taskId, String projectId);

    void clear();

}
